# 简介

本压缩包是一个maven工程, eclipse/idea均可按maven项目导入

MainLauncher是入口,启动即可

## 环境要求

* 必须JDK8+
* eclipse或idea等IDE开发工具,可选

## 配置信息位置

数据库配置信息,jetty端口等配置信息,均位于src/main/resources/application.properties

默认端口是8080, 访问 http://127.0.0.1:8080 就对了

## 命令下启动

使用mvn命令即可

```
// for windows
set MAVEN_OPTS="-Dfile.encoding=UTF-8"
mvn clean compile nutzboot:run

// for *uix
export MAVEN_OPTS="-Dfile.encoding=UTF-8"
mvn clean compile nutzboot:run
```

## 打包成发布文件

打包成独立运行的jar文件

```
mvn clean package nutzboot:shade
```

打包成war, 仅web项目
```
mvn clean package nutzboot:shade nutzboot:war
```

## 相关资源
论坛: https://nutz.cn
官网: https://nutz.io
一键生成NB的项目: https://get.nutz.io

## 本demo的已知问题

### 自身版本号有可能显示"未知"

能否能读取自身版本号,取决于编译/运行的方式

- `eclipse下直接跑main方法` 没有版本信息
- `idea下直接跑main方法` 没有版本信息
- `mvn compile nutzboot:run` 有版本号信息
- `mvn compile nutzboot:shade` 版本号信息固化在jar里面
