package htdi.nutz.helloworld;

import java.io.IOException;

import org.nutz.Nutz;
import org.nutz.boot.NbApp;
import org.nutz.ioc.impl.PropertiesProxy;
import org.nutz.ioc.loader.annotation.*;
import org.nutz.lang.Strings;
import org.nutz.lang.util.NutMap;
import org.nutz.mvc.annotation.*;

@IocBean
public class MainLauncher {
    
    @Inject
    protected PropertiesProxy conf;

    @At("/")
    @Ok("jst:/index.html")
    public NutMap index(String who) throws IOException {
        NutMap re = new NutMap();
        re.put("who", Strings.sBlank(who, "world"));
        re.put("self_version", conf.get("app.build.version", "未知"));
        re.put("nutz_version", Nutz.version());
        return re;
    }

    public static void main(String[] args) throws Exception {
        new NbApp().run();
    }

}
